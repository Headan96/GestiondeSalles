package epsi.GestionSalles;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import epsi.GestionSalles.application.GestionReservation;
import epsi.GestionSalles.application.GestionReservationImplementation;
import epsi.GestionSalles.application.GestionSalleImplementation;
import epsi.GestionSalles.application.GestionUserImplementation;
import epsi.GestionSalles.domain.KeyRepository;
import epsi.GestionSalles.domain.Reservation;
import epsi.GestionSalles.domain.ReservationRepository;
import epsi.GestionSalles.domain.SalleRepository;
import epsi.GestionSalles.domain.User;
import epsi.GestionSalles.domain.UserRepository;


@SpringBootApplication
public class GestionSallesApplication {
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	ReservationRepository reservationRepository;
	
	@Autowired
	SalleRepository salleRepository;
	
	@Autowired
	KeyRepository keyRepository;
	

	public static void main(String[] args) {
		SpringApplication.run(GestionSallesApplication.class, args);
	}
	
	@Bean
	public CommandLineRunner demo1(ReservationRepository repository, SalleRepository repositorysalle) {
		return (args) -> {
			
			System.out.println("----------------------------");
			System.out.println("--------Instructions--------");
			System.out.println("----------------------------");
			
			GestionUserImplementation client1 = new GestionUserImplementation();
			client1.creationClient(1, "Devars", "Lucien", "lulu@gmail.com", "06224897356", "password", true);
			
			GestionSalleImplementation salle1 = new GestionSalleImplementation(repositorysalle);
			salle1.creationSalles("1er", 156, "A58B2");
			
			GestionReservation resa1 = new GestionReservationImplementation(repository);
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			Date date = df.parse("19/07/1996");
			resa1.creationResa(date, 60, 156, 1, 1);
			
			Reservation r = resa1.getReservation(1);
			System.out.println(r);
			
			System.out.println("----------------------------");
			System.out.println("------Fin Instructions------");
			System.out.println("----------------------------");
		};
	}
}



