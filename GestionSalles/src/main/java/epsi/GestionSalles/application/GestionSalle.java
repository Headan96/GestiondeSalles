package epsi.GestionSalles.application;

import epsi.GestionSalles.domain.Reservation;
import epsi.GestionSalles.domain.Salle;

public interface GestionSalle {
	public void creationSalles(String etage, int numero, String code);
	public Salle getReservation(int numero);
}
