package epsi.GestionSalles.application;

import epsi.GestionSalles.domain.User;

public class GestionUserImplementation implements GestionUser{

	@Override
	public void creationClient(int IdUser, String nom, String prenom, String mail, String telephone, String password, Boolean admin) {
		User user = new User();
		user.setNom(nom);
		user.setPrenom(prenom);
		user.setMail(mail);
		user.setTelephone(telephone);
		user.setPassword(password);
		user.setIdUser(IdUser);
		user.setAdmin(admin);
	}

}
