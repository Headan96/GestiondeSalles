package epsi.GestionSalles.application;

import java.util.Date;

import epsi.GestionSalles.domain.Reservation;

public interface GestionReservation {
	public Reservation creationResa(Date resa, int duree, int numero, int IdResa, int IdUser);
	public void annulationResa(Date resa, int duree, int numero, int IdResa, int IdUser);
	public Reservation getReservation(int IdResa);

}
