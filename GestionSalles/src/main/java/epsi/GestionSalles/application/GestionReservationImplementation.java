package epsi.GestionSalles.application;

import java.util.Date;

import epsi.GestionSalles.domain.ResaUser;
import epsi.GestionSalles.domain.Reservation;
import epsi.GestionSalles.domain.ReservationRepository;
import epsi.GestionSalles.domain.Salle;
import epsi.GestionSalles.domain.User;

public class GestionReservationImplementation implements GestionReservation{
	
	ReservationRepository resaRepository; 
	public GestionReservationImplementation(ReservationRepository resaRepository) {
		super();
		this.resaRepository = resaRepository;
	}

	@Override
	public Reservation creationResa(Date resa, int duree, int numero, int IdResa, int IdUser) {
		
		Salle salle= new Salle();
		salle.setNumero(numero);
		
		User user = new User();
		user.setIdUser(IdUser);
		
		
		Reservation reservation = new Reservation();
		reservation.setIdResa(IdResa);
		reservation.setDate(resa);
		reservation.setDuree(duree);
		reservation.addSalle(salle);
		resaRepository.save(reservation);
		
		return reservation;
		
	}

	public void annulationResa(Date resa, int duree, int numero, int IdResa, int IdUser) {
		Salle salle= new Salle();
		salle.setNumero(numero);
		
		User user = new User();
		user.setIdUser(IdUser);
		
		Reservation reservation = new Reservation();
		reservation.setIdResa(IdResa);
		reservation.setDate(null);
		reservation.setDuree(0);
		reservation.removeSalle(salle);
		
	}

	@Override
	public Reservation getReservation(int IdResa) {
		return resaRepository.findById(IdResa).get();
	}
	
}
