package epsi.GestionSalles.application;

import epsi.GestionSalles.domain.Key;
import epsi.GestionSalles.domain.Salle;
import epsi.GestionSalles.domain.SalleRepository;

public class GestionSalleImplementation implements GestionSalle{
	
	SalleRepository salleRepository;

	public GestionSalleImplementation(SalleRepository salleRepository) {
		super();
		this.salleRepository = salleRepository;
	}

	@Override
	public void creationSalles(String etage, int numero, String code) {
		
		Key key = new Key();
		key.setCode(code);
		
		Salle salle = new Salle();
		salle.setEtage(etage);
		salle.setNumero(numero);
		salleRepository.save(salle);
	
	}

	@Override
	public Salle getReservation(int numero) {
		// TODO Auto-generated method stub
		return salleRepository.findById(numero).get();
	}
	
}
