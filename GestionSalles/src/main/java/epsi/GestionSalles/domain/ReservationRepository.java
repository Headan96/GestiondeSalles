package epsi.GestionSalles.domain;

import org.springframework.data.repository.CrudRepository;

import epsi.GestionSalles.application.GestionReservation;
import epsi.GestionSalles.application.GestionUserImplementation;

public interface ReservationRepository extends CrudRepository<Reservation, Integer>{
	

}
