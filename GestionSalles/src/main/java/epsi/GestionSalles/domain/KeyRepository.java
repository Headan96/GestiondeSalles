package epsi.GestionSalles.domain;

import org.springframework.data.repository.CrudRepository;

public interface KeyRepository extends CrudRepository<Key, Integer>{

}
