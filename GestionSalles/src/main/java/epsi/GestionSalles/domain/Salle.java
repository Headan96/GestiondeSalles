package epsi.GestionSalles.domain;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;


@Entity
public class Salle {
	
	@OneToMany(mappedBy="salle", cascade = CascadeType.ALL)
	Collection<Key> keys = new ArrayList<Key>();
	
	@ManyToOne
	Reservation resa;
	
	String etage;
	@Id
	int numero;
	

	public Collection<Key> getKeys() {
		return keys;
	}

	public void setKeys(Collection<Key> keys) {
		this.keys = keys;
	}

	public Reservation getResa() {
		return resa;
	}

	public void setResa(Reservation resa) {
		this.resa = resa;
	}
	
	public String getEtage() {
		return etage;
	}

	public void setEtage(String etage) {
		this.etage = etage;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	@Override
	public String toString() {
		return "la clef = " + keys + ", l'etage = " + etage + " et le numero=" + numero + "";
	}



}
