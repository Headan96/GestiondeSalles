package epsi.GestionSalles.domain;

import org.springframework.data.repository.CrudRepository;

public interface SalleRepository extends CrudRepository<Salle, Integer>{

}
