package epsi.GestionSalles.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class ResaUser {
	
	@ManyToOne
	User user;
	@ManyToOne
	Reservation reservation;
	
	@Id
	int idResaUser;
	
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Reservation getReservation() {
		return reservation;
	}
	public void setReservation(Reservation reservation) {
		this.reservation = reservation;
	}	

}
