package epsi.GestionSalles.domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Reservation {
	
	@OneToMany(mappedBy="resa", cascade=CascadeType.ALL)
	Collection<Salle> salles = new ArrayList<Salle>();
	@OneToMany(mappedBy="reservation", cascade=CascadeType.ALL)
	Collection<ResaUser> users = new ArrayList<ResaUser>();
	
	Date seance;
	int duree;
	@Id
	int idResa;
	
	
	public Collection<ResaUser> getUsers() {
		return users;
	}
	public void setUsers(Collection<ResaUser> users) {
		this.users = users;
	}
	public void addUser(ResaUser user) {
		users.add(user);
		user.setReservation(this);
	}
	public void removeUser(ResaUser user) {
		users.remove(user);
		user.setReservation(null);
	}
	
	public int getIdResa() {
		return idResa;
	}
	public void setIdResa(int idResa) {
		this.idResa = idResa;
	}
	public Collection<Salle> getSalles() {
		return salles;
	}
	public void setSalles(Collection<Salle> salles) {
		this.salles = salles;
	}
	public void addSalle(Salle salle) {
		salles.add(salle);
		salle.setResa(this);
	}
	public void removeSalle(Salle salle) {
		salles.remove(salle);
		salle.setResa(null);
	}
	
	public Date getDate() {
		return seance;
	}
	public void setDate(Date resa) {
		this.seance = resa;
	}
	
	public int getDuree() {
		return duree;
	}
	public void setDuree(int duree) {
		this.duree = duree;
	}
	
	@Override
	public String toString() {
		return "La reservation " + idResa + " a les utilisateurs suivants : " + users + ".\n"
				+ "Cette réservation privatise la salle avec "+ salles +" pour le "+ seance +" pendant "+ duree +" minutes";
	}

}
