package epsi.GestionSalles.domain;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class User {
	
	@OneToMany(mappedBy="user")
	Collection<ResaUser> reservations = new ArrayList<ResaUser>();
	
	@Id
	int idUser;
	String nom;
	String prenom; 
	Boolean admin;
	String mail;
	String telephone;
	String password; 
	
	
	public Boolean getAdmin() {
		return admin;
	}

	public void setAdmin(Boolean admin) {
		this.admin = admin;
	}

	public Collection<ResaUser> getReservations() {
		return reservations;
	}

	public void setReservations(Collection<ResaUser> reservations) {
		this.reservations = reservations;
	}

	public int getIdUser() {
		return idUser;
	}

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	@Override
	public String toString() {
		return "User : nom=" + nom + ", prenom=" + prenom + ", mail=" + mail + ", telephone="
				+ telephone + "";
	}

}
