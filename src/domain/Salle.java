package salles;

import java.util.ArrayList;
import java.util.Collection;

public class Salle {
	
	Collection<User> users = new ArrayList<User>();
	Key key;
	Reservation resa;
	
	String etage;
	String numero;
	
	public Collection<User> getUsers(){
		return users;
	}
	
	public void setUsers (Collection<User> users) {
		this.users = users;
	}
	
	public Key getKey() {
		return key;
	}

	public void setKey(Key key) {
		this.key = key;
	}

	public Reservation getResa() {
		return resa;
	}

	public void setResa(Reservation resa) {
		this.resa = resa;
	}

	public void addUser(User user) {
		users.add(user);
		user.setSalle(this);
	}
	
	public void removeUser(User user) {
		users.remove(user);
		user.setSalle(null);
	}
	
	public String getEtage() {
		return etage;
	}

	public void setEtage(String etage) {
		this.etage = etage;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	@Override
	public String toString() {
		return "Salle [users=" + users + ", key=" + key + ", etage=" + etage + ", numero=" + numero + "]";
	}

}
