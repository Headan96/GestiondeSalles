package salles;

import application.salle.GestionUser;

public class User {
	
	Salle salle;
	String nom;
	String prenom; 
	String mail;
	String telephone;
	
	public Salle getVoiture() {
		return salle;
	}
	
	public void setSalle(Salle salle) {
		this.salle = salle;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	@Override
	public String toString() {
		return "User : nom=" + nom + ", prenom=" + prenom + ", mail=" + mail + ", telephone="
				+ telephone + "";
	}
}
