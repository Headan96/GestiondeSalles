package salles;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

public class Reservation {
	
	Salle salle;
	
	Date seance;
	int duree;
	
	
	public Salle getSalle() {
		return salle;
	}
	public void setSalle(Salle salle) {
		this.salle = salle;
	}
	public Date getDate() {
		return seance;
	}
	public void setDate(Date resa) {
		this.seance = resa;
	}
	public int getDuree() {
		return duree;
	}
	public void setDuree(int duree) {
		this.duree = duree;
	}
	
	@Override
	public String toString() {
		return "Reservation [salle=" + salle + ", seance=" + seance + ", duree=" + duree + "]";
	}
	
	
}
