package salles;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Main {

	public static void main(String[] args) {
		
		try {
		
			Salle ferrari = new Salle();
			ferrari.setEtage("1er");
			ferrari.setNumero(202);
			
			User tintin = new User();
			tintin.setNom("Milou");
			tintin.setPrenom("Tintin");
			tintin.setMail("Tintin");
			tintin.setTelephone(065);
			ferrari.addUser(tintin);
			
			User haddock = new User();
			haddock.setNom("Perse");
			haddock.setPrenom("Haddock");
			haddock.setMail("Haddock@gmail.com");
			haddock.setTelephone(012);
			ferrari.addUser(haddock);
			
			Reservation resa1 = new Reservation();
			
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			Date seance = df.parse("30/05/2018");
			resa1.setResa(seance);
			resa1.setDuree(10);
			
			Key key1 = new Key();
			key1.setCode(123);
			
			ferrari.setKey(key1);
			key1.setSalle(ferrari);
			
			resa1.setSalle(ferrari);
			
			System.out.println(ferrari);
			
			ferrari.removeUser(tintin);
			
			System.out.println(ferrari);
			
			System.out.println(resa1);
			
			System.out.println(key1);
			
			
		
		} catch(Exception e) {
			e.printStackTrace();
		}

	}

}
