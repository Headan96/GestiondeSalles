package salles;

import java.util.ArrayList;
import java.util.Collection;

public class ResaUser {
	
	User user;
	Reservation reservation;
	Collection<User> users = new ArrayList<User>();
	Collection<Reservation> reservations = new ArrayList<Reservation>();
	
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Reservation getReservation() {
		return reservation;
	}
	public void setReservation(Reservation reservation) {
		this.reservation = reservation;
	}
	public Collection<User> getUsers() {
		return users;
	}
	public void setUsers(Collection<User> users) {
		this.users = users;
	}
	public Collection<Reservation> getReservations() {
		return reservations;
	}
	public void setReservations(Collection<Reservation> reservations) {
		this.reservations = reservations;
	}
	
	

}
