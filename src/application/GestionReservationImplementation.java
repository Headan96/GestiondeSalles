package application.salle;

import java.util.Date;

import salles.Reservation;
import salles.Salle;

public class GestionReservationImplementation implements GestionReservation{

	@Override
	public void CreationResa(Date resa, int duree, Salle salle) {
		Reservation reservation = new Reservation();
		reservation.setDate(resa);
		reservation.setDuree(duree);
		reservation.setSalle(salle);
		
	}

	@Override
	public void AnnulationResa(Date resa, int duree, Salle salle) {
		Reservation reservation = new Reservation();
		reservation.setDate(null);
		reservation.setDuree(0);
		reservation.setSalle(null);
		
	}
	
}
