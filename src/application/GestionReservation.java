package application.salle;

import java.util.Date;

import salles.Salle;

public interface GestionReservation {
	public void CreationResa(Date resa, int duree, Salle salle);
	public void AnnulationResa(Date resa, int duree, Salle salle);
}
